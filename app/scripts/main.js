import 'babel-polyfill';
import Pivot from './pivot'

import testData from './data';

function sum(array) {
  return array.reduce((prev, curr) => {
    return parseInt(prev) + parseInt(curr.price)
  }, 0)
}

function count(array) {
  return array.length
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateNewCar() {
  const brand = getRandomInt(0, 2);

  return {
    year: Math.floor(getRandomInt(2014, 2016)),
    department: ['NY', 'Moscow', 'Tokyo'][getRandomInt(0, 2)],
    brand: ['Toyota', 'Honda', 'Nissan'][brand],
    model: [
      ['Camry', 'Corolla', 'Rav4', 'Highlander'],
      ['Accord', 'Civic', 'CR-V', 'Pilot'],
      ['Teana', 'Pulsar', 'X-Trail', 'Pathfinder']
    ][brand][getRandomInt(0, 3)],
    type: ['sedan', 'suv'][getRandomInt(0, 1)],
    price: getRandomInt(15000, 50000)
  }
}

function renderPivot() {
  new Pivot(testData, ['year', 'department'], ['brand', 'type'], document.getElementById('cars_sum'), sum);
  new Pivot(testData, ['year', 'department'], ['brand', 'model'], document.getElementById('cars_count'), count);
};

renderPivot();
document.querySelector('.js-add-cars').addEventListener('click', function(){
  for (let i = 0; i < 50000; i++)
    testData.push(generateNewCar());
  
  renderPivot();
})
