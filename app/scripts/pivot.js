// UTILS -------
// chaining setAttribute
Element.prototype.cSetAttribute = function () {
  this.setAttribute.apply(this, arguments)
  return this;
}

// return this against new HTMLElement
Node.prototype.tAppendChild = function () {
  this.appendChild.apply(this, arguments)
  return this;
}

Node.prototype.addClass = function (cls) {
  if (!this.classList.contains(cls)) {
    this.classList.add(cls)
  }

  return this;
}

Node.prototype.removeClass = function (cls) {
  this.classList.remove(cls)
  return this;
}

NodeList.prototype.addClass = function (cls) {
  for (let i = 0; i < this.length; i++) {
    this[i].addClass(cls);
  }
}

NodeList.prototype.removeClass = function (cls) {
  for (let i = 0; i < this.length; i++) {
    this[i].removeClass(cls);
  }
}
// -----------

export default class {
  constructor(data, cols, rows, table, fn) {
    var property = {
      data,
      cols,
      rows,
      fn,
      table
    }

    this._property = property;
    this.createStructure(this.renderAggregate);

    table.addEventListener('mouseover', ev => {
      if (ev.target.tagName === 'TD' && ev.target.getAttribute('data-cell')) {
        var verticalCellIndex = ev.target.getAttribute('data-cell').split('-')[1];
        var horizontalCellIndex = ev.target.getAttribute('data-cell').split('-')[0];

        table.querySelectorAll('td').removeClass('hover');
        table.querySelectorAll(`[data-cell$="-${verticalCellIndex}"]`).addClass('hover')
        table.querySelectorAll(`[data-cell^="${horizontalCellIndex}-"]`).addClass('hover')
      }
    })
  }

  *[Symbol.iterator]() {
    for (let entry of this._pivotResult.entries()) {
      var cols = {},
        rows = {},
        entryKey = JSON.parse(entry[0]);

      for (let col of this._property.cols) cols[col] = entryKey[col];
      for (let row of this._property.rows) rows[row] = entryKey[row];

      yield {
        cols,
        rows,
        aggregate: this._property.fn(entry[1])
      };
    }
  }

  _pick(obj, args) {
    var result = {};
    for (let arg in obj)
      if (obj.hasOwnProperty(arg) && args.indexOf(arg) > -1)
        result[arg] = obj[arg]

    return result;
  }

  createStructure(fn) {
    const table = this._property.table,
      fields = [].concat(this._property.cols, this._property.rows);
    var tmpRow;

    table.innerHTML = '';

    function createTableStructure() {
      tmpRow = table.insertRow()
        .insertCell()
        .cSetAttribute('colspan', this._property.cols.length)
        .cSetAttribute('rowspan', this._property.rows.length)
        .addClass('header')
        .parentNode
        .insertCell().tAppendChild(document.createTextNode(this._property.rows[0]))
        .cSetAttribute('data-content', this._property.rows[0])
        .cSetAttribute('data-type', 'row')
        .addClass('header')

      for (let rIndex = 1; rIndex < this._property.rows.length; rIndex++)
        table.insertRow()
        .insertCell().tAppendChild(document.createTextNode(this._property.rows[rIndex]))
        .cSetAttribute('data-content', this._property.rows[rIndex])
        .addClass('header')

      tmpRow = table.insertRow();
      for (let cIndex = 0; cIndex < this._property.cols.length; cIndex++)
        tmpRow.insertCell().tAppendChild(document.createTextNode(this._property.cols[cIndex]))
        .cSetAttribute('data-content', this._property.cols[cIndex])
        .addClass('header')

      tmpRow.insertCell().addClass('header');
    }

    function lazyProcess(obj) {
      var onlyRowsObject = this._pick(obj, this._property.rows),
        onlyColsObject = this._pick(obj, this._property.cols);

      // GROUPING ------------
      var mapObj = JSON.stringify(this._pick(obj, fields)),
        arrInMap = groups.has(mapObj) ? groups.get(mapObj) : [];
      arrInMap.push(obj);
      groups.set(mapObj, arrInMap);

      // FILL HORIZONTAL -----
      maybeStacked.length = 0;
      path.length = 0;

      fullyIndentical = true;
      for (let row in onlyRowsObject) {
        path.push(onlyRowsObject[row])
        if (table.querySelector(`[data-content="${path.join('-')}"]`)) {
          maybeStacked.push(table.querySelector(`[data-content="${path.join('-')}"]`));
          continue
        };
        fullyIndentical = false;
        lastCell = table.querySelector(`[data-content="${row}"]`).parentNode
          .insertCell().tAppendChild(document.createTextNode(onlyRowsObject[row]))
          .cSetAttribute('data-content', path.join('-'))
          .addClass('horizontal');
      }

      if (lastCell) {
        lastCell.rowSpan = 2;
        lastCell
          .cSetAttribute('data-index', cIndex++)
          .cSetAttribute('valign', 'bottom')
        lastCell = null;
        horizontalCellsLength++;
      }

      if (!fullyIndentical)
        for (let stacked of maybeStacked)
          stacked.colSpan += 1;

      // FILL VERTICAL -----
      maybeStacked.length = 0;
      path.length = 0;

      tmpRow = table.insertRow().cSetAttribute('data-vertical', true);
      fullyIndentical = true;
      for (let col in onlyColsObject) {
        path.push(onlyColsObject[col])
        if (table.querySelector(`[data-content="${path.join('-')}"]`)) {
          maybeStacked.push(table.querySelector(`[data-content="${path.join('-')}"]`));
          continue
        };
        fullyIndentical = false;
        lastCell = tmpRow.insertCell().tAppendChild(document.createTextNode(onlyColsObject[col]))
          .cSetAttribute('valign', 'top')
          .cSetAttribute('align', 'right')
          .cSetAttribute('data-content', path.join('-'))
          .addClass('vertical');
      }

      if (lastCell) {
        lastCell.colSpan = 2;
        lastCell.cSetAttribute('data-index', rIndex++);
        lastCell = null;
      }

      if (!fullyIndentical)
        for (let stacked of maybeStacked)
          stacked.rowSpan += 1;
      else table.deleteRow(-1)
    }

    createTableStructure.call(this);

    var maybeStacked = [],
      fullyIndentical,
      lastCell,
      path = [],
      horizontalCellsLength = 0,
      cIndex = 0,
      rIndex = 0;

    const groups = new Map(),
      self = this;
    var queue = this._property.data.concat().reverse(), startLen = queue.length;
    this.renderPreload();

    (function iterator() {
      setTimeout(() => {
        var end = +new Date() + 50;
        do {
          lazyProcess.call(self, queue.pop());
        } while (queue.length > 0 && end > +new Date());

        if (queue.length > 0) {
          iterator();
          table.nextElementSibling.innerHTML = (100 - queue.length / startLen * 100).toFixed(2) + '%';
        } else {          
          // FILL ZERO --------------          
          table.querySelectorAll('[data-vertical]').forEach((row, rowIndex) => {
            for (let cellIndex = 0; cellIndex < horizontalCellsLength; cellIndex++)
              row.insertCell().cSetAttribute('data-cell', rowIndex + '-' + cellIndex)
              .appendChild(document.createTextNode('-'));
          })

          table.style.display = 'block';
          table.nextElementSibling.parentNode.removeChild(table.nextElementSibling);
          self._pivotResult = groups;

          if (fn) fn.call(self);
        };
      }, 10)
    })()

  }

  renderPreload() {
    var preload = document.createElement('div');
    this._property.table.style.display = 'none';    
    preload.style.textAlign = 'center';
    preload.style.width = this._property.table.offsetWidth && this.table.offsetWidth + 'px' || 'auto';
    preload.style.lineHeight = this._property.table.offsetHeight && this.table.offsetHeight + 'px' || 'auto';
    preload.style.top = this._property.table.offsetTop && this.table.offsetTop + 'px' || 0
    preload.style.left = this._property.table.offsetLeft && this.table.offsetLeft + 'px' || 0
    this._property.table.insertAdjacentHTML('afterend', preload.outerHTML);
  }

  renderAggregate() {
    const table = this._property.table;
    let index = 0;

    for (let agg of this) {
      var cols_req = [], rows_req = [];
      for (let col of this._property.cols) cols_req.push(agg.cols[col]);
      for (let row of this._property.rows) rows_req.push(agg.rows[row]);

      const horz = table.querySelector(`[data-content="${cols_req.join('-')}"]`)
      const top = table.querySelector(`[data-content="${rows_req.join('-')}"]`)      
      table.querySelector(`[data-cell="${horz.getAttribute('data-index')}-${top.getAttribute('data-index')}"]`)
        .addClass('content')
        .innerHTML = agg.aggregate;
    }
  }
}
